import 'dart:convert';
import 'dart:async' show Future;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class Me extends StatefulWidget {
  @override
  _MeState createState() => _MeState();
}

Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/fake/json_me_list.json');
}

class ItemBean {
  String linkName;
  String linkPhoto;
  bool showDividerLine=true;
  bool showDividerGroup=false;
  @override
  String toString() {
    return "{name:" + this.linkName + "  value:" + linkPhoto + "}";
  }
}

class _MeState extends State<Me> {
  List<ItemBean> _itemList = List();

  @override
  void initState() {
    _loadJson();
    super.initState();
  }

  void _loadJson() {
    loadAsset().then((value) {
      List<ItemBean> result = new List();
      JsonDecoder decoder = new JsonDecoder();
      var json = decoder.convert(value);
      if (json.length > 0) {
        for (var object in json) {
          for (int i=0;i<object.length;i++) {
            var v = object[i];
            ItemBean itemBean = new ItemBean();
            itemBean.linkName = v['linkName'];
            itemBean.linkPhoto = v['linkPhoto'];
            if(i==object.length-1){
              itemBean.showDividerGroup=true;
              itemBean.showDividerLine=false;
            }else{
              itemBean.showDividerGroup=false;
              itemBean.showDividerLine=true;
            }
            result.add(itemBean);
          }
        }
      }
      setState(() {
        _itemList = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> itemViews = List();
    itemViews.add(HeaderSection());
    itemViews.add(new Container(height: 10, color: Color(0xfff5f5f5)));
    for (var i = 0; i < _itemList.length; i++) {
      itemViews.add(ItemSection(_itemList[i]));
    }
    return Scaffold(
      body:
          ListView(children: itemViews, padding: const EdgeInsets.symmetric()),
    );
  }
}

class ItemSection extends StatelessWidget {
  final ItemBean itemBean;

  ItemSection(this.itemBean);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[Container(
        height: 50,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15),
              child: Image.network(
                itemBean.linkPhoto,
                fit: BoxFit.cover,
              ),
              height: 20,
              width: 20,
            ),
            Expanded(
                child: Container(
              child: Text(itemBean.linkName),
              margin: EdgeInsets.only(left: 10),
            )),
            Container(
                margin: EdgeInsets.only(right: 15),
                child: ImageIcon(
                  AssetImage("assets/images/icon_arrow_right2.png"),
                  color: Color(0xffbbbbbb),
                  size: 11,
                ))
          ],
        )), Container(height: itemBean.showDividerGroup?10:itemBean.showDividerLine?0.5:0,color: itemBean.showDividerGroup?Color(0xfff5f5f5):itemBean.showDividerLine?Color(0xffeeeeee):Colors.transparent,)]);
  }
}

class HeaderSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 160,
        child: Stack(
          children: <Widget>[
            Image.asset("assets/images/bg_my_top.png",
                height: 160, fit: BoxFit.cover),
            Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 15),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(
                          "http://filesystem1.bbtree.com/2018/05/08/410be87b1b144d4800aab8fbe0962051/And/1525773793556-1244746321.webp@200h_200w_1e_1c"),
                      radius: 28,
                      foregroundColor: Colors.white,
                    )),
                Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left: 8),
                        child: Column(
                            children: <Widget>[Text("大头鬼",style: TextStyle(color: Colors.white,fontSize: 15),), Text("暂无签名~",style: TextStyle(color: Colors.white,fontSize: 13))],
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center))),
                Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ImageIcon(
                      AssetImage("assets/images/icon_arrow_right2.png"),
                      color: Colors.white,
                      size: 11,
                    ))
              ],
              crossAxisAlignment: CrossAxisAlignment.center,
            )
          ],
        ));
  }
}
