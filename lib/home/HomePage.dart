import 'package:bbtree_flutter/home/ClassCircle.dart';
import 'package:bbtree_flutter/home/Discovery.dart';
import 'package:bbtree_flutter/home/GrowUp.dart';
import 'package:bbtree_flutter/home/Me.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  List<Widget> pages = new List();
  int _currentIndex = 1;

  @override
  void initState() {
    pages.add(ClassCircle());
    pages.add(GrowUp());
    pages.add(Discovery());
    pages.add(Me());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Image.asset("assets/images/main_circle_off.png",width: 30.0,height: 30.0),
              activeIcon: Image.asset('assets/images/main_circle_on.png',
                  width: 30.0,height: 30.0),
              title: Text("班级圈"),
              ),
          BottomNavigationBarItem(
              icon:Image.asset('assets/images/main_growup_off.png',width: 30.0,height: 30.0),
              activeIcon: Image.asset('assets/images/main_growup_on.png',width: 30.0,height: 30.0),
              title: Text("成长")),
          BottomNavigationBarItem(
              icon: Image.asset('assets/images/main_find_off.png',width: 30.0,height: 30.0),
              activeIcon: Image.asset('assets/images/main_find_on.png',width: 30.0,height: 30.0),
              title: Text("发现")),
          BottomNavigationBarItem(
              icon: Image.asset('assets/images/main_my_off.png',width: 30.0,height: 30.0),
              activeIcon: Image.asset('assets/images/main_my_on.png',width: 30.0,height: 30.0),
              title: Text("我"))
        ],
        currentIndex: _currentIndex,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        type: BottomNavigationBarType.fixed,
      ),
      body: pages[_currentIndex],
    );
  }

}
