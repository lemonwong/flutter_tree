import 'package:bbtree_flutter/home/discovery/AttentionPage.dart';
import 'package:bbtree_flutter/home/discovery/ListenPage.dart';
import 'package:flutter/material.dart';

class Discovery extends StatefulWidget {
  @override
  _DiscoveryState createState() => _DiscoveryState();
}

class _DiscoveryState extends State<Discovery>
    with SingleTickerProviderStateMixin {
  var _tabController;
  var channelList = ["关注", "推荐", "听听", "看看", "会员专区", "圈子", "更多"];
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: channelList.length);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 5),
                                child: Image.asset(
                              "assets/images/icon_cell_search.png",
                              width: 20,
                              height: 20,
                            )),
                            Text(
                              '团购 | 绘本 | 故事',
                              style: TextStyle(
                                  fontSize: 16, color: const Color(0xffcccccc)),
                            )
                          ],
                        ),
                        height: 32,
                        margin: EdgeInsets.only(left: 15),
                        padding: EdgeInsets.only(left: 15, right: 15),
                        decoration: BoxDecoration(
                          color: Color(0xfff5f5f5),
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 15, right: 15),
                        child: Image.asset(
                            "assets/images/find_img_red_cross.png",
                            width: 24,
                            height: 24,
                            fit: BoxFit.cover))
                  ],
                ),
              ),
              TabBar(
                isScrollable: true,
                labelStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                unselectedLabelStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                labelColor: Color(0xff28d19d),
                unselectedLabelColor: Color(0xff666666),
                indicatorSize: TabBarIndicatorSize.label,
                indicatorWeight: 2,
                indicatorColor: Color(0xff28d19d),
                tabs: channelList.map<Tab>((String t) {
                  return Tab(
                    text: t,
                  );
                }).toList(),
                controller: _tabController,
              )
            ],
          ),
        ),
        body: Container(
            child: TabBarView(
                controller: _tabController,
                children: channelList.map<Widget>((String t) {
                  if("关注"==t){
                    return AttentionPageWidget();
                  }else if("听听"==t){
                    return ListenPage();
                  }
                  return Center(child: Text(t,style: TextStyle(fontSize: 24),));
                }).toList())));
  }
}

class MyAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget child;

  const MyAppBar({@required this.child}) : assert(child != null);

  @override
  _MyAppBarState createState() => _MyAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(42 + kTextTabBarHeight);
}

class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(child: widget.child),
    );
  }
}
