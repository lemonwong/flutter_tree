import 'package:bbtree_flutter/common/SplashScreen.dart';
import 'package:bbtree_flutter/home/HomePage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          accentColor: Color(0xff28d19d),
          accentTextTheme: TextTheme(
              body1: TextStyle(color: Color(0xff333333), fontSize: 14))),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/HomePage': (context) {
          return new HomePage();
        }
      },
    );
  }
}
