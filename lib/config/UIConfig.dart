
import 'package:flutter/material.dart';

class UIDimensions{

static const int commonPadding = 15;

}

class UIColors{

  static const Color primaryColor = Color(0xff28d19d);

  static const Color textColor1 = Color(0xff333333); 
  static const Color textColor2 = Color(0xff666666); 
  static const Color textColor3 = Color(0xff999999); 
}