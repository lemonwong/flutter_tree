import 'package:flutter/material.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2),() => Navigator.of(context).pushReplacementNamed('/HomePage'));
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Container(
        width: 88,
        height: 35,
        color: Colors.white,
        child: Column(children: <Widget>[
          Expanded(
              child: Stack(
            children: <Widget>[
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                        width: 36,
                        height: 36,
                        child: CircularProgressIndicator()),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Text(
                          '加载中...',
                          style: themeData.accentTextTheme.body1,
                        ))
                  ])
            ],
          )),
          Container(
            height: 70,
            alignment: Alignment.center,
            child: GestureDetector(
              child: Image.asset('assets/images/logo_loading.png',
                  width: 88, height: 35),
            ),
          )
        ]));
  }
}
