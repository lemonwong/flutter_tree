class TimestampUtil {
  static String getTimeState(String timestamp) {
    DateTime now = DateTime.now();
    DateTime t = DateTime.parse(timestamp);
    int nowY = now.year;
    int nowMonth = now.month;
    int nowDay = now.day;

    int tY = t.year;
    int tMonth = t.month;
    int tDay = t.day;

    if (nowY == tY && nowMonth == tMonth) {
      if (nowDay == tDay) {
        int ss = now.millisecondsSinceEpoch - t.millisecondsSinceEpoch;
        if (ss < 60000) {
          return "刚刚";
        } else if (ss < 3600000) {
          return "${ss/1000/60}分钟前";
        }else{
           return "${ss/1000/60/60}小时前";
        }
      } else if (nowDay == tDay + 1) {
        return "昨天";
      } else {
        return "${nowDay - tDay}天";
      }
    }
    return timestamp;
  }
}
