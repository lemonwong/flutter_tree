package com.example.bbtreeflutter;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "com.bbtree.flutter/aes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);

        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @SuppressLint("NewApi")
            @Override
            public void onMethodCall(final MethodCall methodCall, final MethodChannel.Result result) {
                final String key = methodCall.argument("key");
                final String src = methodCall.argument("src");
                AsyncTask.SERIAL_EXECUTOR.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String value = null;
                            if (methodCall.method.equals("encrypt")) {
                                value = AES.Encrypt(key, src);
                            } else if (methodCall.method.equals("decrypt")) {
                                value = AES.Decrypt(key, src);
                            }
                            result.success(value);
                        } catch (Exception e) {
                            e.printStackTrace();
                            result.error("aes encrypt error", null, null);
                        }
                    }
                });
            }
        });
    }
}
